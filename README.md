### Launch
`
sbt run 
`


### API
`GET /account` - create new account

return account ID
```
{"id":"6b53d525-7204-4561-9d9c-2dc9ed2bd200"}

```

`GET /account/all` - show created accounts

return array of account ids 
```
[{"id":"1cd77cb2-ea5c-4538-9d39-d39107a13dc8"},
{"id":"bb490bc0-337f-4695-a9f1-dad71d452231"},
{"id":"334de1d6-609c-4cb7-a9d9-cb79d18959fd"},
{"id":"4e3ef554-48aa-4309-991c-639c6258ed35"}]
```

`GET /balances` - show accounts balances

return array of balances 
```
{"balances":[
{"account":"7736879c-4b51-4b6f-b181-2fc3b6e1eda1",
"balance":3300},
{"account":"a43dc16d-5750-43b0-a5be-dafd7145d1fc",
"balance":0}
]}
```


`POST /transaction` ```{"deposit": ACCOUNT-ID, "volume": VOLUME }``` - deposit _VOLUME_ to account _ACCOUNT-ID_ 
return balance for deposit account 

``` 
{"balances":[{"account":"7736879c-4b51-4b6f-b181-2fc3b6e1eda1","balance":3300}]}
```


`POST /transaction` `{"from": "_ACCOUNT-1_", "to" : "ACCOUNT-2", "volume": VOLUME }` - transfer _VOLUME_ from account with ID _ACCOUNT-ID-1_ to account with ID _ACCOUNT-ID-2_

return balances for both accounts 

``` 
{"balances":[{"account":"7736879c-4b51-4b6f-b181-2fc3b6e1eda1","balance":3300}]}
```