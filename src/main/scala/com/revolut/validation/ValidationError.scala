package com.revolut.validation

import java.util.UUID

sealed abstract class ValidationError(txId: UUID, cause: String) {
  val message: String = s"Invalid transaction $txId: $cause"
}

case class AccountNotFound(txId: UUID, accountId: UUID)
  extends ValidationError(txId, s"account $accountId not found")

case class NegativeAmount(txId: UUID, amount: Long)
  extends ValidationError(txId, s"negative amount $amount of asset")

case class NotEnoughAmount(txId: UUID, accountId: UUID, amount: Long)
  extends ValidationError(txId, s"not enough amount $amount of asset on $accountId")

case class SameAccountTransfer(txId: UUID, accountId: UUID)
  extends ValidationError(txId,s"transfer to same account $accountId")