package com.revolut.validation


import com.revolut.model._

object TransactionValidator {

  def validate(transaction: Transaction, state: AccountState): Either[ValidationError, Unit] = {

    val txId = transaction.id
    transaction match {
      case _: Deposit =>
        if (transaction.volume < 0) {
          Left(NegativeAmount(txId, transaction.volume))
        } else {
          Right()
        }

      case Transfer(_, from, to, volume, _) =>
        if (transaction.volume < 0) {
          Left(NegativeAmount(txId, transaction.volume))
        } else if (from.id == to.id) {
          Left(SameAccountTransfer(txId, accountId = to.id))
        } else if (state.account == from && Transaction.calcBalance(from, state.transactions) - volume < 0) {
          Left(NotEnoughAmount(txId, accountId = from.id, amount = volume))
        } else {
          Right()
        }
    }
  }
}
