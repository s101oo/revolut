package com.revolut

import java.util.UUID
import java.util.concurrent.ConcurrentHashMap

import com.revolut.model.{Account, Transaction, TransactionSet}

import scala.collection.JavaConverters._
import scala.collection.SortedSet
import scala.collection.concurrent.{Map => MMap}

class Datastore {

  private val accountStore: MMap[UUID, Account] = new ConcurrentHashMap[UUID, Account]().asScala
  private val transactionStore: MMap[Account, SortedSet[Transaction]] = new ConcurrentHashMap[Account, SortedSet[Transaction]]().asScala

  def accounts() = accountStore.values.toSeq

  def put(account: Account): Account = {
    accountStore.put(account.id, account)
    account
  }

  def transactions(account: Account): SortedSet[Transaction] = {
    transactionStore.getOrElse(account, TransactionSet.empty)
  }

  def putTxForState(account: Account, newTransaction: Transaction, existing: SortedSet[Transaction]): Boolean = {
    if (existing.isEmpty) {
      transactionStore.putIfAbsent(account, TransactionSet(newTransaction)).isEmpty
    } else {
      transactionStore.replace(account, existing, existing + newTransaction)
    }
  }
}