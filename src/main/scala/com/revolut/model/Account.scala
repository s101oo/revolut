package com.revolut.model

import java.util.UUID
import java.util.concurrent.locks.ReentrantLock

import com.revolut.Datastore

case class Account(id: UUID){
  val lock = new ReentrantLock()
}

object Account {
  def create(): Account = Account(UUID.randomUUID())
}

class AccountService(datastore: Datastore) {
  def create(): Account =  datastore.put(Account.create())
  def accounts() = datastore.accounts()
}