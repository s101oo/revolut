package com.revolut.model

import java.util.UUID

import scala.collection.SortedSet

sealed trait Transaction {
  def id: UUID
  def to: Account
  def volume: Long
  def timestamp: Long
  def accounts: Set[Account]
}

case class Deposit(
  id: UUID,
  to: Account,
  volume: Long,
  timestamp: Long) extends Transaction {
  val accounts = Set(to)
}

case class Transfer(
  id: UUID,
  from: Account, to: Account,
  volume: Long,
  timestamp: Long) extends Transaction {
  val accounts = Set(from, to)
}

object Transaction {
  private def now(): Long = System.currentTimeMillis()

  def transfer(from: Account, to: Account, volume: Long): Transfer =
    Transfer(UUID.randomUUID(), from, to, volume, now())

  def deposit(to: Account, volume: Long): Deposit =
    Deposit(UUID.randomUUID(), to, volume, now())

  def calcBalance(account: Account, transactions: SortedSet[Transaction]): Long = {
    transactions.foldLeft(0L) { (balance, tx) =>
      tx match {
        case Deposit(_, _, volume, _) =>
          balance + volume

        case Transfer(_, from, _, volume, _) =>
          if (account == from) balance - volume
          else balance + volume
      }
    }
  }
}

object TransactionSet {
  private val ordering: Ordering[Transaction] =
    Ordering.fromLessThan((t1, t2) => t1.timestamp <= t2.timestamp)

  val empty = SortedSet.empty[Transaction](ordering)

  def apply(transaction: Transaction)  = SortedSet(transaction)(ordering)
}