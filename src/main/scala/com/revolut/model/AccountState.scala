package com.revolut.model

import scala.collection.SortedSet

case class AccountState(account: Account, transactions: SortedSet[Transaction])
object AccountState {
  def empty(account: Account) = AccountState(account, TransactionSet.empty)
}