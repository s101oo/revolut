package com.revolut.util

import java.util.concurrent.locks.Lock

import com.revolut.model.Account

object Locking {

  def lockedBy[A, B](lock: Lock)(f: => B): B = {
    lock.lock()
    val res = f
    lock.unlock()
    res
  }

  def locksOrderByUUID(a1: Account, a2: Account): (Lock, Lock) = {
    val compare = a1.id.compareTo(a2.id)
    if (compare == -1 ) (a1.lock, a2.lock)
    else (a2.lock, a1.lock)
  }
}