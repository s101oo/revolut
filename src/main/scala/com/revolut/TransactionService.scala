package com.revolut

import com.revolut.model._
import com.revolut.util.Locking
import com.revolut.validation.{AccountNotFound, TransactionValidator, ValidationError}

import scala.annotation.tailrec

class TransactionService(private val datastore: Datastore) {

  def currentBalance(account: Account): Long = {
    val transactions = datastore.transactions(account)
    Transaction.calcBalance(account, transactions)
  }

  def accountState(account: Account): AccountState = {
    AccountState(account, datastore.transactions(account))
  }

  def putForState(transaction: Transaction, state: AccountState): Boolean = {
    datastore.putTxForState(state.account, transaction, state.transactions)
  }

  def process(transaction: Transaction): Either[ValidationError, Unit] = {

    @tailrec
    def processLoop(tx: Transaction): Either[ValidationError, Unit] = {
      val updateResult =
        tx match {
          case deposit @ Deposit(_, to, _, _) =>
            val state = accountState(to)
            TransactionValidator.validate(tx, state).map { _ =>
              putForState(deposit, state)
            }

          case transfer @ Transfer(_, from, to, _, _) =>

            val fromState = accountState(from)
            val toState = accountState(to)

            val validation = for {
              _ <- TransactionValidator.validate(transfer, fromState)
              _ <- TransactionValidator.validate(transfer, toState)
            } yield ()

            validation.map { _ =>
              val (firstLock, secondLock) = Locking.locksOrderByUUID(from, to)

              Locking.lockedBy(firstLock){
                Locking.lockedBy(secondLock){
                  putForState(transfer, fromState) &&
                    putForState(transfer, toState)
                }
              }
            }
        }

      updateResult match {
        case Right(replaced) =>
          if (replaced) Right()
          else processLoop(tx)
        case Left(err) => Left(err)
      }
    }

    val validAccounts = transaction.accounts.foldLeft(Right(): Either[ValidationError, Unit]) { (v, acc) =>
      v.flatMap{ _ =>
        if (!datastore.accounts().contains(acc)) Left(AccountNotFound(transaction.id, acc.id))
        else Right()
      }
    }

    for {
      _ <- validAccounts
      _ <- processLoop(transaction)
    } yield ()
  }

}