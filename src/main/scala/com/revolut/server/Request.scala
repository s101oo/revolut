package com.revolut.server

import java.util.UUID

import com.revolut.validation.ValidationError

object Request {
  case class Deposit(deposit: UUID, volume: Long)
  case class Transfer(to: UUID, from: UUID, volume: Long)
}

trait Response {
  def error: Boolean
}

object Response {
  trait SuccessResponse extends Response {
    val error = false
  }

  case class Balance(account: UUID, balance: Long)
  case class Balances(balances: Balance*) extends SuccessResponse

  case class ErrorResponse(message: String, error: Boolean = true) extends Response

  def fromError(error: ValidationError) = {
    ErrorResponse(error.message)
  }
}
