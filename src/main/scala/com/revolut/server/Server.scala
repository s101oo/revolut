package com.revolut.server

import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.revolut.TransactionService
import com.revolut.model.{Account, AccountService, Transaction}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._

import scala.concurrent.ExecutionContext

class Server(
  accountService: AccountService,
  transactionService: TransactionService,
  val port: Int,
  val host: String)(
  implicit
  system: ActorSystem,
  materializer: ActorMaterializer,
  ec: ExecutionContext
) {

  private def balances(accountIds: UUID*) = {
    val balances = accountIds.map { id =>
      val balance = transactionService.currentBalance(Account(id))
      Response.Balance(id,balance)
    }
    Response.Balances(balances:_*)
  }

  private val accountRoute =
    pathPrefix("account") {
      (pathEnd & get) {
        complete(StatusCodes.Created -> accountService.create())
      } ~
        (path("all") & get) {
          complete(StatusCodes.OK -> accountService.accounts())
        }
    }

  private val transactionRoute =
    (pathPrefix("transaction") & post) {
        entity(as[Request.Transfer]) { request =>
          complete {
            val transfer = Transaction.transfer(
              from = Account(request.from),
              to = Account(request.to),
              volume = request.volume)

            transactionService.process(transfer)
              .map(_ => balances(request.from, request.to))
              .left.map(error =>
                StatusCodes.BadRequest -> Response.fromError(error))
          }
        } ~
          entity(as[Request.Deposit]) { request =>
            complete {
              val deposit = Transaction.deposit(Account(request.deposit), request.volume)
              transactionService.process(deposit)
                .map(_ => (StatusCodes.OK, balances(request.deposit)))
                .left.map(error =>
                StatusCodes.BadRequest -> Response.fromError(error))
            }
          }
    }

  private val balanceRoute =
    (pathPrefix("balances") & get) {
      complete {
        balances(accountService.accounts().map(_.id).toSeq: _ *)
      }
    }

  private val route = accountRoute ~ transactionRoute ~ balanceRoute

  def bind() = {
    Http().bindAndHandle(route, host, port)
  }

}
