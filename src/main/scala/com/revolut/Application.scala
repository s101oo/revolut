package com.revolut

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.revolut.model.AccountService
import com.revolut.server.Server

import scala.io.StdIn

object Application {
  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val datastore = new Datastore
    val server = new Server(
      new AccountService(datastore),
      new TransactionService(datastore),
      host = "localhost", port = 9000)

    val binding = server.bind()
    println(s"Server start on ${server.host}:${server.port}")
    StdIn.readLine()
    binding
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}
