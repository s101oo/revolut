package com.revolut

import com.revolut.model.{Account, Transaction}
import org.scalacheck.Gen
import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.PropertyChecks

class TransactionProcessingSpec extends FreeSpec with PropertyChecks with Matchers {

  "One deposit" in {
    forAll(Gen.posNum[Long]){ depositVolume =>

      val datastore = new Datastore
      val to = Account.create()
      datastore.put(to)
      val deposit = Transaction.deposit(to, depositVolume)

      val txService = new TransactionService(datastore)

      txService.process(deposit) should be('right)
      txService.currentBalance(to) shouldEqual depositVolume
    }
  }

  "One deposit and one transfer" in {

    val depositAndTransferVolumes = for {
      f <- Gen.chooseNum[Long](1, Long.MaxValue)
      s <- Gen.chooseNum[Long](0, f)
    } yield (f, s)

    forAll(CommonGens.datastoreGenWithTwoAccs, depositAndTransferVolumes) { case ((datastore, from, to), (depositVolume, transferVolume)) =>

      val deposit = Transaction.deposit(from, depositVolume)
      val transfer = Transaction.transfer(from, to, transferVolume)

      val txService = new TransactionService(datastore)

      txService.process(deposit) should be('right)
      txService.process(transfer) should be('right)
      txService.currentBalance(transfer.from) shouldEqual (deposit.volume - transfer.volume)
      txService.currentBalance(transfer.to) shouldEqual transfer.volume
    }

  }
}
