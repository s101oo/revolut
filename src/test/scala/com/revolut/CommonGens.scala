package com.revolut

import com.revolut.model.Account
import org.scalacheck.Gen

object CommonGens {
  val accountGen = Gen.uuid.map(Account(_))

  val datastoreGenWithTwoAccs = for {
    account1 <- accountGen
    account2 <- accountGen
    datastore <- Gen.const(new Datastore)
    _ = datastore.put(account1)
    _ = datastore.put(account2)
  } yield (datastore, account1, account2)

}
