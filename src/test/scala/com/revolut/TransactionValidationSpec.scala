package com.revolut

import com.revolut.model.{Account, AccountState, Transaction}
import com.revolut.validation._
import org.scalacheck.Gen
import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.PropertyChecks

class TransactionValidationSpec extends FreeSpec with PropertyChecks with Matchers {

  "Invalid transactions" in {

    forAll(CommonGens.datastoreGenWithTwoAccs, Gen.negNum[Long]) { case ((_, _, account), negative) =>
      val deposit = Transaction.deposit(account, negative)

      val validationRes = TransactionValidator.validate(
        deposit,
        AccountState.empty(account))

      validationRes shouldEqual Left(NegativeAmount(deposit.id, negative))
    }

    forAll(CommonGens.datastoreGenWithTwoAccs, Gen.posNum[Long]) { case ((_, from, to), amount) =>
      val transfer = Transaction.transfer(from, to, amount)

      val validationRes = for {
        _ <- TransactionValidator.validate(transfer, AccountState.empty(from))
        _ <- TransactionValidator.validate(transfer, AccountState.empty(to))
      } yield ()

      validationRes shouldEqual Left(NotEnoughAmount(transfer.id, from.id, amount))
    }

    forAll(CommonGens.datastoreGenWithTwoAccs, Gen.posNum[Long]) { case ((_, _, account), amount) =>
      val transfer = Transaction.transfer(account, account, amount)

      val validationRes = TransactionValidator.validate(
        transfer,
        AccountState.empty(account))

      validationRes shouldEqual Left(SameAccountTransfer(transfer.id, account.id))
    }
  }

  forAll(CommonGens.datastoreGenWithTwoAccs, Gen.posNum[Long]) { case ((datastore, _, _), amount) =>

    val account = Account.create()
    val transaction = Transaction.deposit(account, amount)
    val service = new TransactionService(datastore)
    service.process(transaction) shouldEqual Left(AccountNotFound(transaction.id, account.id))
  }

}
